var http = require('http');
var fs = require('fs');
var functions = require('./functions.js').functions;

exports.admins = 
[
    "124136556578603009",
    "193093226096361472"
]

exports.cmds = 
[
    { //Parse dice in a string
        cmd: "find",
        params: "resource name, your latitude, your longitude",
        execute:function(bot, info, args)
        {
            if(args.length == 0)
            {
                var names = functions.resourceNames();

                bot.sendMessage({
                    to: info.channelID,
                    message: "Available resources: `" + names.join("`, `") + "`.",
                    typing: false
                }, function()
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Use `?find <resource name> <latitude> <longitude>` to find resources closest to you.",
                        typing: false
                    }); 
                });

                return;
            }

            resourceArr = [];

            for(var i = 0; i < args.length; i++)
            {
                if(isNaN(args[i]))
                {
                    resourceArr.push(args[i]);
                }
            }

            for(var i = args.length - 1; i >= 0; i--)
            {
                if(isNaN(args[i]))
                {
                    args.splice(i, 1);
                }
            }

            var sightings = null;

            if(args.length > 0)
            {
                sightings = functions.findResources(resourceArr.join(" "), args[0], args[1]);
            }
            else
            {
                sightings = functions.findResources(resourceArr.join(" "), 0, 0);
            }

            if(!sightings)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Resource '" + args[0] + "' not found!",
                    typing: false
                });
            }
            else
            {
                str = "";

                var max = 5;

                if(max > sightings.length)
                    max = sightings.length;

                for(var i = 0; i < max; i++)
                {
                    var raftAccessible = sightings[i].raftAccessible;
                    var needFly = sightings[i].flightNeeded;
                    
                    var needStr = "Can be reached on foot.";

                    if(needFly)
                    {
                        needStr = "Requires flight.";
                    }
                    else if (raftAccessible)
                    {
                        needStr = "Acessible by raft.";
                    }

                    var threats = sightings[i].threats.join(", ");

                    if(sightings[i].threats.length == 0)
                    {
                        threats = "None";
                    }

                    str += "```Lat: " + sightings[i].location.lat + " Lon: " + sightings[i].location.lon + "\n" 
                    + needStr + "\n"
                    + "Threats in area: " + threats + "\n"
                    + "\n" + sightings[i].description + " ``` "
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Found resources:", //Send this message first for a slight delay, instant messages may be blocked by spam-prevention measures
                    typing: true
                }, function()
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: str,
                        typing: false
                    });
                }); 
            }
            
        }
    },
    { //EXAMPLE COMMAND DEMONSTRATING BOT SPEECH, COMMAND PARAMETERS AND BOT MEMORY
        cmd: "remember",
        params: "thing to remember (optional. Omit to see last thing remembered.)",
        execute:function(bot, info, args)
        {
            if(args.length == 0) //Did the user add any parameters?
            { //            --No parameters
                bot.addReaction({
                    channelID: info.channelID,
                    messageID: info.evt.d.id,
                    reaction: "❤"
                });

                bot.sendMessage({
                    to: info.channelID,
                    message: "Remembered _\"" + bot.data[info.serverId].reminder + "\"_", //Read bot memory
                    typing: true
                }); 
            }
            else //Yes parameters
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Remembering _\"" + args.join(" ") + "\"_!",
                    typing: false
                });

                bot.data[info.serverId].reminder = args.join(" "); //SAVE SOMETHING INTO BOT MEMORY (args.join(" ") turns the arguments into a single bit of text)
            }
        }
    },
    { //Parse dice in a string
        cmd: "roll",
        params: "dice string (1d20+5)",
        execute:function(bot, info, args)
        {
            var diceStr = args.join(" ");

            bot.sendMessage({
                to: info.channelID,
                message: parseTextDice(diceStr),
                typing: true
            });
        }
    },
    { //Display the top five most @mentioned users in the server
        cmd: "topmentions",
        params: "none",
        execute: function(bot, info, args)
        {
            var mentions = Object.entries(bot.data[info.serverId].mentions);

            mentions.sort(function(a, b){return b[1] - a[1]});

            if(mentions.length > 5)
                mentions.splice(5, mentions.length - 5);

            var str = "Top 5 Mentioned Users: \n";

            for(var i = 0; i < mentions.length; i++)
            {
                str += (i + 1) + ": <@!" + mentions[i][0] + "> at " + mentions[i][1] + (mentions[i][1] == 1? " mention!\n" : " mentions!\n");
            }

            bot.sendMessage({
                to: info.channelID,
                message: str,
                typing:false
            });
        }
    },

    //////////////////////////////////////////////////////////////////////
    ////////    DEFAULT COMMANDS BELOW, DON'T EDIT UNLESS   //////////////
    ////////    YOU REALLY KNOW WHAT YOU'RE DOING           //////////////
    //////////////////////////////////////////////////////////////////////

    {
        cmd: "help",
        params: "none",
        execute: function(bot, info, args)
        {
            var cmds = ["Commands:"];
            var wip = ["Work In Progress (may not work correctly or at all):"]
            
            for(var i = 0; i < bot.commands.length; i++)
            {
                if(!bot.commands[i].hidden)
                {
                    var lines = "";

                    for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                        lines += "-";

                    if(!bot.commands[i].wip)
                        cmds.push ("?" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                    else
                        wip.push ("?" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: "Commands: ```" + cmds.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                typing: false
            });
        }
    },
    {
        cmd: "debughelp",
        params: "none",
        hidden: true,
        execute: function(bot, info, args)
        {
            var cmds = ["Commands:"];
            var hidden = ["Admin-only commands:"];
            
            for(var i = 0; i < bot.commands.length; i++)
            {
                var lines = "";

                for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                    lines += "-";

                if(!bot.commands[i].hidden)
                    cmds.push ("?" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                else
                    hidden.push ("?" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
            }

            bot.sendMessage({
                to: info.channelID,
                message: "Admins: <@" + exports.admins.join("> <@") + "> ```" + /*cmds.join("\n") + "\n\n" + */ hidden.join("\n") + "```",
                typing: false
            });
        }
    },
    {
        cmd: "serverid",
        params: "none",
        hidden: true,
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Server ID: `" + bot.data[info.serverId].serverId + "`",
                typing: false
            });
        }
    },
    {
        cmd: "invitelink",
        params: "none",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "https://discordapp.com/oauth2/authorize?&client_id=479575060345257986&scope=bot&permissions=511040",
                typing: false
            });
        }
    },
    {
        cmd: "ping",
        params: "none",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "<@!" + info.userID + ">" + ' Pong!',
                typing: true
            });
        }
    }, 
    {
        cmd:"update",
        params: "none",
        hidden:true,
        execute:function(bot, info, args)
        {
            var exec = require('child_process').exec;

            bot.sendMessage({
                to: info.channelID,
                message: "Fetching changes...",
                typing: false
            }, function()
            {
                exec('git fetch --all', (err, stdout, stderr) => 
                {
                    exec('git log --oneline master..origin/master', (err, stdout, stderr) => 
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: (stdout == ""? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                            typing: false
                        }, function()
                        {
                            bot.suicide();
                        });
                    });
                });
            });
        }
    },
    {
        cmd: "echo",
        params: "anything",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: args.join(" "),
                typing: true
            });
        }
    },
    {
        cmd: "rawecho",
        params: "anything",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + args.join(" ") + "```",
                typing: true
            });
        }
    },
    {
        cmd: "memdump",
        params: "none",
        hidden: true,
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + JSON.stringify(bot.data[info.serverId]) + "```",
                typing: false
            });
        }
    },
    {
        cmd: "play",
        params: "'playing' string",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 0
                }
            });
            
            bot.sendMessage({
                to: info.channelID,
                message: "_Now playing " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "stream",
        params: "'streaming' string",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 1
                }
            });
            
            bot.sendMessage({
                to: info.channelID,
                message: "_Now streaming " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "listento",
        params: "'listening to' string",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 2
                }
            });
            
            bot.sendMessage({
                to: info.channelID,
                message: "_Now listening to " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "watch",
        params: "'watching' string",
        execute:function(bot, info, args)
        {
            bot.setPresence({game:
                {
                    name:args.join(" "),
                    type: 3
                }
            });

            bot.sendMessage({
                to: info.channelID,
                message: "_Now watching " + args.join(" ") + "_",
                typing:false
            })
        }
    },
    {
        cmd: "restartpc",
        params: "none",
        hidden: true,
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec("shutdown.exe -r -f -t 0", (err, stdout, stderr) => console.log(stdout))
        }
    },
    {
        cmd: "lastreload",
        params: "none",
        hidden: true,
        execute: function(bot, info, args)
        {
            fs.readFile('./lastreboot.txt', function read(err, data) {
                if (err) {
                    data = "Unknown";
                }
                
                bot.sendMessage({
                    to: info.channelID,
                    message: data,
                    typing:false
                });
            });
        }
    },
    {
        cmd: "logdump",
        params: "nothing",
        hidden: true,
        execute:function(bot, info, args)
        {
            var url = "plog.txt";
            //exec("type plog.txt > tlog.txt;type log.txt >> tlog.txt", (err, stdout, stderr) => 
            //{
                bot.uploadFile({
                    to: info.channelID,
                    file: "crashlog.txt"
                }, function(error, response)
                {
                    bot.uploadFile({
                        to: info.channelID,
                        file: "currentlog.txt"
                    }, function(error, response)
                    {
                        console.log(error);
                    })
                })
            //})
        }
    },
    {
        cmd: "exec",
        params: "command",
        hidden: true,
        execute: function(bot, info, args)
        {
            var command = args.join(" ");

            var exec = require('child_process').exec
            exec(command, (err, stdout, stderr) => 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + stdout + "```",
                    typing:false
                })
            })
        }
    },
    {
        cmd: "version",
        params: "none",
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec('git log --oneline -n 1 HEAD', (err, stdout, stderr) => 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "Current version: ```" + stdout + "```",
                    typing:false
                })
            })
        }
    }
]

function parseTextDice(text)
{
    var cont = true;
    var r = /(\d+)d(\d+)/;

    while(cont)
    {
        var matches = text.match(r);
        
        console.log(matches);

        if(matches === null)
        {
            cont = false;
            break;
        }
        
        if(matches.length < 2)
        {
            cont = false;
            break;
        }	
        
        a = matches[1] * rand(1, matches[2] - -1);
        
        text = text.replace(r, a);
    }

    cont = true;
    r = /(\d+)([\/|\*])(\d+)/;
    while(cont)
    {
        var matches = text.match(r);
        
        console.log(matches);

        if(matches === null)
        {
            cont = false;
            break;
        }
        
        if(matches.length < 3)
        {
            cont = false;
            break;
        }	
        
        if(matches[2] == "*")
            a = matches[1] * matches[3];
        else
            a = matches[1] / matches[3];
        
            text = text.replace(r, a);
    }

    cont = true;
    r = /(\d+)([\+|\-])(\d+)/;
    while(cont)
    {
        var matches = text.match(r);
        
        if(matches === null)
        {
            cont = false;
            break;
        }
        
        if(matches.length < 3)
        {
            cont = false;
            break;
        }	
        
        if(matches[2] == "+")
            a = matches[1] - -matches[3];
        else
            a = matches[1] - matches[3];
        
        text = text.replace(r, a);	
    }

    return text;
}

function rand(min, max)
{
    return min + (Math.floor(Math.random() *(max - min)));
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}