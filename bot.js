var Discord = require('discord.io');
var auth = require('./auth.json');
var fs = require('fs');

var cmdsFile = require('./commands.js');
var commands = cmdsFile.cmds;
var admins = cmdsFile.admins;
console.log(commands);

var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

var initialized = false;
bot.commands = commands;

bot.data = null;

bot.saveData = function(callback, context)
{
    if(bot.data != null && bot.data != {} && initialized)
    {
        bot.data = bot.data || {};

        console.log("Making backup before data overwrite...");
        var exec = require('child_process').exec
        exec("copy botData.json botDataBackup.json", (err, stdout, stderr) => 
        {
            console.log(stdout);

            fs.writeFile("botData.json", JSON.stringify(bot.data), function(err) {
                if (err) {
                    console.log(err);
                }
    
                context = context || this;
                callback && callback.call(context, true);
            });
        });
    }
    else
    {
        context = context || this;
        callback && callback.call(context, false);
    }
}

bot.on('ready', function (evt) {
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');
    
    fs.readFile('./botData.json', function read(err, data) {
        if (err) {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch(e)
        {
            if(bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if(bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read(err, data) {
                if (err) {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");
                }
                catch(e)
                {
                    if(bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                    }

                    initialized = true;
                }
            });
        }
        else
        {
            initialized = true;
        }
    });
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`

    if(!initialized)
        return;

    var wasBot = evt.d.author.bot;

    var id = evt.d.guild_id || userID;

    var group = [userID];

    var words = message.split(' ');
    bot.data[id] = bot.data[id] || {};

    if(!wasBot)
    {
        for(var i = 0; i < words.length; i++)
        {
            var allMentions = words[i].match(/(<@)!?([0-9]+)(>)/g);

            if (allMentions && allMentions.length > 0)
            {
                for(var m = 0; m < allMentions.length; m++)
                {
                    var validMention = allMentions[m].match(/(<@)!?([0-9]+)(>)/);
                    if(validMention)
                    {
                        if(group.indexOf(validMention[2]) < 0)
                        {
                            group.push(validMention[2]);
                        }
                    }
                }
            }
        }

        for(var i = 0; i < group.length; i++)
        {
            if(group[i] != userID)
            {
                bot.data[id].mentions = bot.data[id].mentions || {};

                bot.data[id].mentions[group[i]] = (bot.data[id].mentions[group[i]] || 0) + 1;
            }
        }
    }

    if (message.substring(0, 1) == '?') 
    {
        console.log(message);
        var args = message.substring(1).split(' ');
        var cmd = args[0];
       
        var info = {user:user, userID:userID, channelID:channelID, message:message, evt:evt, serverId:id};

        args = args.splice(1);
        for(var i = 0; i < commands.length; i++)
        {
            if(cmd.toLowerCase() == commands[i].cmd)
            {
                if(commands[i].hidden && admins.indexOf(info.userID) == -1)
                    return;

                commands[i].execute(bot, info, args);
                break;
            }
        }
    }

    bot.saveData();
});