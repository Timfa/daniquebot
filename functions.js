var http = require('http');
var fs = require('fs');
var resources = require('./resourcesightings.json');

exports.functions =
{
    resourceNames()
    {
        var names = [];

        for(var i = 0; i < resources.length; i++)
        {
            names.push(resources[i].name.join("/"));
        }

        return names;
    },

    findResources: function(name, lat, lon)
    {
        var pos = new exports.functions.vector2(lat, lon);

        lat = lat || 0;
        lon = lon || 0;

        for(var i = 0; i < resources.length; i++)
        {
            for(var n = 0; n < resources[i].name.length; n++)
            {
                if(resources[i].name[n].toLowerCase() == name.toLowerCase())
                {
                    resources[i].sightings.sort(function(a, b)
                    {
                        var av = new exports.functions.vector2(a.location.lat, a.location.lon);
                        var bv = new exports.functions.vector2(b.location.lat, b.location.lon);
                        return exports.functions.v2distance(av, bv);
                    });

                    return resources[i].sightings;
                }
            }
        }

        return null;
    },

    vector2: function(x, y)
    {
        this.x = x;
        this.y = y;

        this.length = function()
        {
            return Math.sqrt( (this.x * this.x) + (this.y * this.y) );
        }
    },
    
    v2distance: function(a, b)
    {
        var c = new exports.functions.vector2(a.x - b.x, a.y - b.y);
        return c.length();
    }
}